package stefanus.arvin.androidmvpvm;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import stefanus.arvin.androidmvpvm.main.MainViewModel;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MainViewModelTest {
    private MainViewModel mainViewModel;

    @Before
    public void setUp() throws Exception {
        mainViewModel = new MainViewModel();
    }

    @Test
    public void shouldEnableButtonOnSetName() {
        mainViewModel.setNameEditText("Test");

        assertThat(mainViewModel.getNameEditText(), is("Test"));
        assertThat(mainViewModel.buttonEnabled.get(), is(true));
    }

    @Test
    public void shouldDisableTheButtonOnClearName() {
        mainViewModel.clearName();

        assertThat(mainViewModel.getNameEditText(), is(""));
        assertThat(mainViewModel.buttonEnabled.get(), is(false));
    }
}