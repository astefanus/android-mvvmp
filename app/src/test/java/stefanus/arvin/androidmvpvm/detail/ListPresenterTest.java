package stefanus.arvin.androidmvpvm.detail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ListPresenterTest {
    ListPresenter listPresenter;

    @Mock
    ListViewModel listViewModel;

    @Mock
    ListView listView;

    @Before
    public void setUp() throws Exception {
        listPresenter = new ListPresenter(listView, listViewModel, Schedulers.immediate(), Schedulers.immediate());
    }

    @Test
    public void shouldBlablal() throws Exception {
        listPresenter.onCreate();

        verify(listViewModel).hideProgress();
        verify(listViewModel).showProgress();
        verify(listView).notifyChange();
    }
}