package stefanus.arvin.androidmvpvm.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import stefanus.arvin.androidmvpvm.R;
import stefanus.arvin.androidmvpvm.databinding.ActivityMainBinding;
import stefanus.arvin.androidmvpvm.detail.ListActivity;

public class MainActivity extends Activity implements MainView {
    private MainPresenter mainPresenter;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainViewModel viewModel = new MainViewModel();
        mainPresenter = new MainPresenter(this, viewModel);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(viewModel);
    }

    public void onSubmitClick(View v) {
        mainPresenter.onSubmitClick(binding.nameEditText.getText().toString());
    }

    @Override
    public void showWelcomeDialog(String name) {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.welcome_text, name))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mainPresenter.onWelcomeDialogClick();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void launchListActivity() {
        startActivity(new Intent(this, ListActivity.class));
    }
}
