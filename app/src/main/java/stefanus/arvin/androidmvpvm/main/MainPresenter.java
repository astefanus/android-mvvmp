package stefanus.arvin.androidmvpvm.main;

import java.lang.ref.WeakReference;

public class MainPresenter {
    private WeakReference<MainViewModel> mainViewModel;
    private WeakReference<MainView> mainView;

    public MainPresenter(MainView mainView, MainViewModel viewModel) {
        this.mainView = new WeakReference<>(mainView);
        this.mainViewModel = new WeakReference<>(viewModel);
    }

    public void onSubmitClick(String s) {
        String name = getMainViewModel().getNameEditText();
        getMainViewModel().clearName();
        getMainView().showWelcomeDialog(name);
    }

    public void onWelcomeDialogClick() {
        getMainView().launchListActivity();
    }


    private MainViewModel getMainViewModel() {
        return mainViewModel.get();
    }

    private MainView getMainView() {
        return mainView.get();
    }
}
