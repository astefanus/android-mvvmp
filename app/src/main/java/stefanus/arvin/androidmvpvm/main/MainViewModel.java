package stefanus.arvin.androidmvpvm.main;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;

public class MainViewModel extends BaseObservable {
    public final ObservableBoolean buttonEnabled = new ObservableBoolean(false);
    private String nameEditText;

    @Bindable
    public String getNameEditText() {
        return nameEditText;
    }

    public void setNameEditText(String nameEditText) {
        this.nameEditText = nameEditText;
        buttonEnabled.set(!nameEditText.isEmpty());
        notifyChange();
    }

    public void clearName() {
        setNameEditText("");
    }

}
