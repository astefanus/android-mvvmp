package stefanus.arvin.androidmvpvm.main;

public interface MainView {
    void showWelcomeDialog(String name);
    void launchListActivity();
}
