package stefanus.arvin.androidmvpvm.detail;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import stefanus.arvin.androidmvpvm.R;
import stefanus.arvin.androidmvpvm.databinding.ItemListBinding;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private ListPresenter listPresenter;

    public ListAdapter(ListPresenter listPresenter) {
        this.listPresenter = listPresenter;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemListBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_list, parent, false);
        binding.setItemViewModel(new ItemViewModel());
        return new ListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        listPresenter.onBindViewHolder(holder.getBinding().getItemViewModel(), position);
    }

    @Override
    public int getItemCount() {
        return listPresenter.onGetItemCount();
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder {
        private final ItemListBinding binding;

        public ListViewHolder(ItemListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.executePendingBindings();
        }

        public ItemListBinding getBinding() {
            return binding;
        }
    }
}
