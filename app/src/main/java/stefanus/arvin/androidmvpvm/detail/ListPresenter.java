package stefanus.arvin.androidmvpvm.detail;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

public class ListPresenter {
    private final WeakReference<ListViewModel> listViewModel;
    private final WeakReference<ListView> listView;
    private List<Contact> contacts;
    private Scheduler observeOnScheduler;
    private Scheduler subscribeOnScheduler;

    public ListPresenter(ListView listView, ListViewModel listViewModel) {
        this(listView, listViewModel, Schedulers.io(), AndroidSchedulers.mainThread());
    }

    public ListPresenter(
            ListView listView,
            ListViewModel listViewModel,
            Scheduler subscribeOnScheduler,
            Scheduler observeOnScheduler) {
        this.listView = new WeakReference<>(listView);
        this.listViewModel = new WeakReference<>(listViewModel);
        this.subscribeOnScheduler = subscribeOnScheduler;
        this.observeOnScheduler = observeOnScheduler;
    }


    public int onGetItemCount() {
        return contacts == null ? 0 : contacts.size();
    }

    public void onBindViewHolder(ItemViewModel itemViewModel, int position) {
        itemViewModel.setContact(contacts.get(position));
    }

    public void onCreate() {
        Observable.just(Arrays.asList(
                new Contact("Test1", "1234"),
                new Contact("Test2", "1234"),
                new Contact("Test3", "1234"),
                new Contact("Test4", "1234"),
                new Contact("Test5", "1234"),
                new Contact("Test6", "1234"),
                new Contact("Test7", "1234"),
                new Contact("Test8", null)
        ))
//                .delay(2, TimeUnit.SECONDS)
                .observeOn(observeOnScheduler)
                .subscribeOn(subscribeOnScheduler)
                .subscribe(new Subscriber<List<Contact>>() {
                    @Override
                    public void onStart() {
                        getListViewModel().showProgress();
                    }

                    @Override
                    public void onCompleted() {
                        getListViewModel().hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Contact> contacts) {
                        ListPresenter.this.contacts = contacts;
                        getListView().notifyChange();
                    }
                });
    }

    private ListView getListView() {
        return listView.get();
    }

    private ListViewModel getListViewModel() {
        return listViewModel.get();
    }
}
