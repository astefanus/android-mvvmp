
package stefanus.arvin.androidmvpvm.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import stefanus.arvin.androidmvpvm.R;
import stefanus.arvin.androidmvpvm.databinding.ActivityListBinding;

public class ListActivity extends AppCompatActivity implements ListView {

    private ListAdapter adapter;
    private ListPresenter listPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityListBinding activityListBinding = DataBindingUtil.setContentView(this, R.layout.activity_list);
        ListViewModel listViewModel = new ListViewModel();
        activityListBinding.setListViewModel(listViewModel);
        listPresenter = new ListPresenter(this, listViewModel);
        adapter = new ListAdapter(listPresenter);
        RecyclerView recyclerView = activityListBinding.recyclerView;
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

        listPresenter.onCreate();
    }

    @Override
    public void notifyChange() {
        adapter.notifyDataSetChanged();
    }
}
