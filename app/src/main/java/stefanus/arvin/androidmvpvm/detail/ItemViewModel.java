package stefanus.arvin.androidmvpvm.detail;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;

public class ItemViewModel {
    public final ObservableField<String> name = new ObservableField<>();
    public final ObservableField<String> phone = new ObservableField<>();
    public final ObservableInt noContactVisible = new ObservableInt(View.GONE);
    public final ObservableInt phoneNumberVisible = new ObservableInt(View.GONE);

    public void setContact(Contact contact) {
        name.set(contact.getName());
        phone.set(contact.getPhone());

        phoneNumberVisible.set(contact.getPhone() == null ? View.GONE : View.VISIBLE);
        noContactVisible.set(contact.getPhone() == null ? View.VISIBLE : View.GONE);
    }
}
