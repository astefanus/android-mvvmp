package stefanus.arvin.androidmvpvm.detail;

import android.databinding.ObservableInt;
import android.view.View;

public class ListViewModel {

    public final ObservableInt progressBarVisible = new ObservableInt(View.GONE);

    public void showProgress() {
        progressBarVisible.set(View.VISIBLE);
    }

    public void hideProgress() {
        progressBarVisible.set(View.GONE);
    }
}
